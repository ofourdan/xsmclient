.\" $XConsortium: xsmclient.man /main/4 1996/01/26 11:50:14 swick $
.\" Copyright (c) 1994  X Consortium
.\" 
.\" Permission is hereby granted, free of charge, to any person obtaining
.\" a copy of this software and associated documentation files (the
.\" "Software"), to deal in the Software without restriction, including
.\" without limitation the rights to use, copy, modify, merge, publish,
.\" distribute, sublicense, and/or sell copies of the Software, and to
.\" permit persons to whom the Software is furnished to do so, subject to
.\" the following conditions:
.\" 
.\" The above copyright notice and this permission notice shall be included
.\" in all copies or substantial portions of the Software.
.\" 
.\" THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
.\" OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
.\" MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
.\" IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR
.\" OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
.\" ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
.\" OTHER DEALINGS IN THE SOFTWARE.
.\" 
.\" Except as contained in this notice, the name of the X Consortium shall
.\" not be used in advertising or otherwise to promote the sale, use or
.\" other dealings in this Software without prior written authorization
.\" from the X Consortium.
.TH XSMCLIENT 1 "Release 6.1" "X Version 11"
.SH NAME
xsmclient \- X session manager tester
.SH SYNOPSIS
.B xsmclient
[-batch true|false] [-verbose true|false] [-twophase true|false]
.SH DESCRIPTION
.PP
The \fIxsmclient\fP program is used to test the session manager.
.SH OPTIONS
.TP 8
.B \-verbose \fItrue|false\fP
Specifies whether or not detailed protocol information should be
displayed to stdout.  The Xt resource \fIverbose\fP
may also be used to specify this.
.TP 8
.B \-twophase \fItrue|false\fP
Specifies whether single-phase or two-phase saves are performed.
The Xt resource \fItwophase\fP may also be used to specify this.
.TP 8
.B \-batch \fItrue|false\fP
Specifies whether to run in batch mode, i.e. just print the relevant
session management protocols messages and exits (implies \fI-verbose true\fP).
The Xt resource \fIbatch\fP may also be used to specify this.
.SH AUTHOR
Ralph Mor, X Consortium
